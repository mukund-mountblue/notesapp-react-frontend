import React, { useState } from 'react';

import FormControl from 'react-bootstrap/FormControl';
import InputGroup from 'react-bootstrap/InputGroup';
import Button from 'react-bootstrap/Button';

import { authHeaders as headers} from '../services/AuthService';


function NoteAdd({noteBookId, updateView}) {

    let [noteText, setNoteText] = useState("")

    const updateNoteText = (event) => {
        setNoteText(event.target.value);
    }

    const saveNote = () => {
        fetch(`/notebook/${noteBookId}/notes/`, {
            method: 'POST',
            headers,
            body: JSON.stringify({text: noteText})
        })
        .then(() => {
            setNoteText("");
            updateView();
        });
    }


    return (
        <>
            <InputGroup className="mb-3">
                <FormControl
                    placeholder="Add Note"
                    aria-label="Add Note"
                    aria-describedby="basic-addon1"
                    value={noteText}
                    onChange={updateNoteText}
                />
                <InputGroup.Append>
                    <Button variant="outline-secondary" onClick={saveNote}>Save</Button>
                </InputGroup.Append>
            </InputGroup>
        </>    
    )
}

export default NoteAdd;