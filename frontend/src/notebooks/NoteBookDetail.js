import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import NoteList from './NoteList';
import NoteAdd from './NoteAdd';

import { authHeaders as headers} from '../services/AuthService';


function NoteBookDetail(props) {

    let { id } = useParams();

    const [noteBook, setNoteBook] = useState(null);
    const [renderState, setRenderState] = useState(false);

    useEffect(() => {
        fetch(`/notebooks/${id}`, {
            headers,
        })
        .then((res) => res.json())
        .then((json) => setNoteBook(json.data));

    }, [renderState]);
    
    const updateView = () => setRenderState(!renderState);

    if (noteBook != null) {
        return (
            <>
                <Row>
                    <Col>
                        <h2>{noteBook.title}</h2>
                        <p>{noteBook.description}</p>    
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <NoteList notes={noteBook.Notes} updateView={updateView}/>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <NoteAdd noteBookId={noteBook.ID} updateView={updateView} />
                    </Col>
                </Row>
            </>
        );
    } else {
        return <h3>Note book not found</h3>;
    }
};


export default NoteBookDetail;