import React from 'react';
import { MdDeleteForever } from 'react-icons/md';

import { authHeaders as headers} from '../services/AuthService';


function NoteElement({note, updateView}) {

    const delNote = () => {
        fetch(`/notebook/0/notes/${note.ID}`, {
            method: 'DELETE',
            headers,
        })
        .then(() => updateView());
    } 
    return (
        <tr>
            <td>{note.ID}</td>
            <td>{note.text}</td>
            <td><MdDeleteForever onClick={ delNote }/></td>
        </tr>
    )
}

export default NoteElement;