import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import { authHeaders as headers} from '../services/AuthService';


class EditNoteBook extends React.Component {

    
    constructor(props) {
        super(props)
        
        this.state = {
            title: "",
            description: "",
        }
    }

    componentDidMount = () => {

        fetch(`/notebooks/${this.props.match.params.id}`, {
            headers,
        })
        .then(res => res.json())
        .then(json => this.setState(json.data)); 
    }

    handleSubmit = (event) => {
        event.preventDefault();
        
        fetch(`/notebooks/${this.props.match.params.id}`, {
            method: 'PUT',
            headers,
            body: JSON.stringify(this.state)
        })
        .then(() => this.props.history.push("/"))
    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value    
        });
    }

    render() {
        return (
            <>
                <Row>
                    <Col>
                        <h2>Edit {this.state.title}</h2>
                    </Col>
                </Row>
                <Row>
                    <Col>
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Group>
                            <Form.Label>Title of Notebook</Form.Label>
                            <Form.Control type="text" 
                                placeholder="Enter the name of your Note-Book here"
                                name="title"
                                value={this.state.title}
                                onChange={this.handleInputChange}>
                            </Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Description of Notebook</Form.Label>
                            <Form.Control type="text"
                                placeholder="Description of Note Book here"
                                name="description"
                                value={this.state.description}
                                onChange={this.handleInputChange}>
                            </Form.Control>
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>{' '}
                        <Button variant="secondary" href="/">Cancel</Button>
                    </Form>
                    </Col>
                </Row>
            </>
        )
    }
}

export default EditNoteBook;