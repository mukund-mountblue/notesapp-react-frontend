import React from 'react';
import Table from 'react-bootstrap/Table';

import NoteElement from './NoteElement';


function NoteList ({notes, updateView}) {

    let noteEles = notes.map((note) => <NoteElement note={note} updateView={updateView}/>)

    return (
        <Table striped bordered hover size="sm">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Note</th>
                    <th>X</th>
                </tr>
            </thead>
            <tbody>
                {noteEles}
            </tbody>
        </Table>
    )

}

export default NoteList;