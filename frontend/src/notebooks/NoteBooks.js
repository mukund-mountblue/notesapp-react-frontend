import  React, {useEffect, useState} from 'react';
import Row from 'react-bootstrap/Row';
import {Link} from 'react-router-dom';

import NoteBookCard from './NoteBookCard';
import { authHeaders as headers} from '../services/AuthService';


function NoteBooks() {
    const [notebooks, setNotebooks] = useState([]);
    const [renderState, setRenderState] = useState(0);

    useEffect(() => {
        fetch('notebooks/', {
            headers,
        })
        .then(res => res.json())
        .then(json => setNotebooks(json.data))
    }, [renderState]);

    const delNoteBook = (id) => {
        fetch(`notebook/${id}`, {
            method: 'DELETE'
        })
        .then(res => {
            console.log("deleting notebook");
            setRenderState(renderState + 1);
        })
    }


    let notebookEles = notebooks.map((notebook) => <NoteBookCard notebook={notebook} del={delNoteBook} />)

    return (
        <>
            <Row>
                {notebookEles}
            </Row>
        </>
    );
}


export default NoteBooks;