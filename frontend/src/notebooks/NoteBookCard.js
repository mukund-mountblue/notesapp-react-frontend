import React from 'react';

import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';


function NoteBookCard({notebook, del}) {

    const delNoteBook = () => {
        del(notebook.ID)
    }

    return (
        <Col lg={3}>
            <Card style={{ width: '18rem' }}> 
                <Card.Body>
                    <Card.Title>
                        <Link to={`/${notebook.ID}`}>{notebook.title}</Link>
                    </Card.Title>
                    <Card.Text>{notebook.description}</Card.Text>
                    <Button onClick={delNoteBook}>Delete</Button>{' '}
                    <Button variant="secondary" href={`/edit/${notebook.ID}`} >Edit</Button>
                </Card.Body>
            </Card>
        </Col>
    );
}

export default NoteBookCard;