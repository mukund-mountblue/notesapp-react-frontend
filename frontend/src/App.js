import React, { useState } from 'react';
import Container from 'react-bootstrap/Container';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import Nav from 'react-bootstrap/Nav';
import { useHistory } from "react-router-dom";

import NoteBooks from './notebooks/NoteBooks';
import AddNoteBook from './notebooks/AddNoteBook';
import NoteBookDetail from './notebooks/NoteBookDetail';
import EditNoteBook from './notebooks/EditNoteBook';

import './App.css';
import LoginComponent from './auth/LoginComponent';
import SignupComponent from './auth/SignupComponent';
import { logoutUser } from './services/AuthService';


function App() {
  const history = useHistory();
  let [viewState, setViewState] = useState(false);


  const logout = () => {
      logoutUser();
      reset();
  }

  const reset = () => {
    setViewState(!viewState)
  }

  if(window.sessionStorage.getItem('accessToken') == null) {
    return (
      <Container className='p-3'>
        <Router>
          <Switch>
            <Route path="/signup">
              <SignupComponent />
            </Route>
            <Route path="/" >
              <LoginComponent reset={reset} />
            </Route>
          </Switch>
        </Router>
      </Container>
    )
  }

  return (
    <Container className='p-3'>
      <Nav defaultActiveKey="/" as="ul">
        <Nav.Item as="li">
          <Nav.Link href="/">Home</Nav.Link>
        </Nav.Item>
        <Nav.Item as="li">
          <Nav.Link eventKey="link-1" href="/add">Add Notebook</Nav.Link>
        </Nav.Item>
        <Nav.Item as="li">
          <Nav.Link href="" onClick={logout}>Logout</Nav.Link>
        </Nav.Item>
      </Nav>
      <Router>
        <Switch>

          <Route path="/add" component={AddNoteBook}></Route>
          <Route path="/edit/:id" component={EditNoteBook}></Route>
          <Route path="/:id" component={NoteBookDetail}></Route>
          <Route path="/">
            <NoteBooks></NoteBooks>
          </Route>
          
        </Switch>
      </Router>
      
    </Container>
  );
}

export default App;
