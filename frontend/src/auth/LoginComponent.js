import React, { useState } from 'react';

import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Alert from 'react-bootstrap/Alert';

import {useHistory} from 'react-router-dom';
import { loginUser } from '../services/AuthService';


function LoginComponent({reset}) {

    const history = useHistory();
    let [userName, setUserName] = useState("");
    let [password, setPassword] = useState("");
    let [errorMessage, setErrorMessage] = useState("");

    const handleUserNameInput = (event) => setUserName(event.target.value)
    const handlePasswordInput = (event) => setPassword(event.target.value)

    
    const handleSubmit = (event) => {
        event.preventDefault();
        fetch(`/auth/login`, {
            method: 'POST',
            body: JSON.stringify({
                email: userName,
                password: password,
            })
        })
        .then((response) => {
            if(response.status == 200) {
                return response.json();
            } else {
                throw `Error with status ${response.status}`;
            }
        })
        .then((json) => {
            loginUser(json.jwt)
            reset();
        })
        .catch(err => setErrorMessage("Error logging in. Please try again"))
        
    }

    let errorMsgEle = null;

    if(errorMessage != "") {
        errorMsgEle = <Alert variant='danger'>{errorMessage}</Alert>
    }
    return (


        <>
        <Row>
            <h2>Login</h2>
        </Row>
        <Row>
            <Col>
                {errorMsgEle}
            </Col>
        </Row>
        <Row>
            <Col>
                <Form onSubmit={handleSubmit}>
                    <Form.Group>
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="text" 
                            placeholder="Enter your email"
                            name="email"
                            onChange={handleUserNameInput}>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password"
                            placeholder="Enter your password here"
                            name="password"
                            onChange={handlePasswordInput}>
                        </Form.Control>
                    </Form.Group>
                    <Button variant="primary" type="submit">
                        Login 
                    </Button>
                </Form>
            </Col>
        </Row>
        <Row>
            <Col>
            Don't have an account yet?   <Button href="/signup" variant="secondary">Signup Here</Button>{' '}
            </Col>
        </Row>
        </>
    )
}

export default LoginComponent;