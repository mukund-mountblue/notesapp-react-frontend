import React, {useState} from 'react';
import { Col, Form, Row, Button, Alert } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';


function SignupComponent() {

    // pull all the form lifecycle handler into this component
    let { register, handleSubmit, watch, errors } = useForm();
    const history = useHistory();

    let onSubmit = async (data) => {
        try {
            let response = await fetch("/auth/signup", {
                method: 'POST',
                body: JSON.stringify(data),
            });

            history.push("/");
        } catch (err) {
            console.log(err);
        };
    };

    let matchPassword = (value) => {
        return value === watch('password') || "Passwords don't match.";
    };

    let userExists = async (value) => {

        let response = await fetch("/auth/email_exists", {
            method: 'POST',
            body: JSON.stringify({email: watch("email")})
        })
        let json = await response.json();

        if(json.email_exist) {
            return "Email alread exists"
        } else {
            return
        }
    }

    return (
        <>
           <Row>
               <Col>
                    <h3>Sign Up</h3>
               </Col>
           </Row>
           <Row>
               <Col>
                    { errors.confirmPassword && <Alert variant="danger">Passwords do not match</Alert>}
                    <Form onSubmit={handleSubmit(onSubmit)}>            
                        <Form.Group>
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" 
                                placeholder="Enter your email"
                                name="email"
                                required
                                ref={register({ validate: userExists })} />
                            { errors.email && <Alert variant="danger">Email alread exists</Alert>}
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Full Name</Form.Label>
                            <Form.Control type="text"
                                placeholder="Enter your fullname here"
                                name="fullName"
                                required
                                ref={register} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password"
                                placeholder="Enter your password here"
                                name="password"
                                required
                                ref={register} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control type="password"
                                placeholder="Confirm your password here"
                                name="confirmPassword"
                                required
                                ref={register({validate: matchPassword})} />
                        </Form.Group>
                    
                    <Button variant="primary" type="submit">
                        Register
                    </Button>
                </Form>
               </Col>
           </Row>
           <Row>
               <Col>
               Alread have an account?   <Button variant="secondary" href="/">Login Here</Button>{' '}
               </Col>
           </Row>

            
        </>
    )
}

export default SignupComponent;