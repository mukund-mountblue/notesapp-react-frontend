

export const loginUser = (token) => {
    window.sessionStorage.accessToken = token;
}

export const logoutUser = () => {
    window.sessionStorage.removeItem('accessToken');
}


export let authHeaders = {
    'Content-Type': 'application-json',
    'Authorization': `Bearer ${window.sessionStorage.getItem('accessToken')}`,
}