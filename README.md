# notesapp-react-frontend

The frontend for the notes app written in React

## models

### User
Fields
1.  email
2.  password
3.  full_name

### Notebook
Fields
1.  ID
2.  title
3.  description
4.  notes -> List of note 

### Note
1. ID
2. text


## API

### Auth
- POST   /auth/email_exists        
- POST   /auth/signup              
- POST   /auth/login 

### Notes
- POST   /notebook/:notebookId/notes 
- DELETE /notebook/:notebookId/notes/:id

### Notebook
- GET    /notebooks/               
- GET    /notebooks/:id            
- POST   /notebooks/               
- DELETE /notebooks/:id            
- PUT    /notebooks/:id            
